var path = require('path');
var webpack = require('webpack');

module.exports = {
	//devtool: 'inline-source-map',
	devtool: 'eval',
	entry: [
		//'babel-polyfill',
		'webpack-dev-server/client?http://localhost:8080',
		'webpack/hot/only-dev-server',
		'./src/main'
	],
	output: {
		path: path.resolve(__dirname, './dist'),
		filename: 'bundle.js',
		publicPath: '/dist/'
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin(),
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development')
			}
		})
	],
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				loaders: ['react-hot', 'babel'],
				include: [path.resolve(__dirname, 'src')]
			},
			{
				test: /\.less$/,
				loader: "style!css!less"
			}
		]
	}
};