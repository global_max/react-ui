import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

class AutoSuggest extends React.Component {

  static propTypes = {
    dataSource: PropTypes.array.isRequired,
    dataSourceConfig: PropTypes.object,
  };

  static defaultProps = {
    dataSource: [],
    dataSourceConfig: {
      text: 'text',
      value: 'value',
    },
    disableFocusRipple: true,
    filter: (searchText, key) => searchText !== '' && key.indexOf(searchText) !== -1,
    open: false,
    openOnFocus: false,
    onUpdateInput: () => {},
    onNewRequest: () => {},
    searchText: '',
    maxSearchResults: 10,
    menuCloseDelay: 300
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      focusTextField: true,
      isOpen: false,
      searchText: undefined
    };
  }

  componentDidMount() {

  }

  componentWillReceiveProps(nextProps) {
    if (this.props.searchText !== nextProps.searchText) {
      this.setState({
        searchText: nextProps.searchText,
      });
    }
  }

  handleEscKeyDown() {
    this.close();
  };

  handleKeyDown(event) {
    if (this.props.onKeyDown) this.props.onKeyDown(event);

    switch (event.key) {
      case 'Enter':
        this.close();
        const searchText = this.state.searchText;
        
        if (searchText !== '') {
          this.props.onNewRequest(searchText, -1);
        }
        
        break;

      case 'Escape':
        this.close();
        
        break;

      case 'ArrowDown':
        event.preventDefault();
        
        this.setState({
          isOpen: true,
          focusTextField: false,
//           anchorEl: ReactDOM.findDOMNode(this.refs.searchTextField),
        });
        
        break;

      default:
        break;
    }
  };

  handleChange(event) {
    const searchText = event.target.value;

    if (searchText === this.state.searchText) {
      return;
    }

    this.setState({
      searchText: searchText,
      isOpen: true,
//       anchorEl: ReactDOM.findDOMNode(this.refs.searchTextField),
    }, () => {
      this.props.onUpdateInput(searchText, this.props.dataSource);
    });
  };

  handleBlur(event) {
    if (this.state.focusTextField) {
      this.close();
    }

    if (this.props.onBlur) {
      this.props.onBlur(event);
    }
  };

  handleFocus(event) {
    if (!this.state.open && (this.props.triggerUpdateOnFocus || this.props.openOnFocus)) {
      this.setState({
        isOpen: true,
        anchorEl: ReactDOM.findDOMNode(this.refs.searchTextField),
      });
    }

    this.setState({
      focusTextField: true,
    });

    if (this.props.onFocus) {
      this.props.onFocus(event);
    }
  };

  handleMouseDown(searchText) {
    //console.log(event.target);
    
    this.setState({
      searchText: searchText,
      isOpen: false,
//       anchorEl: ReactDOM.findDOMNode(this.refs.searchTextField),
    }, () => {
      this.props.onUpdateInput(searchText, this.props.dataSource);
    });
  }
  
  render() {
    const { dataSource, dataSourceConfig, maxSearchResults } = this.props;
    const { isOpen, searchText } = this.state;

    const requestsList = [];
    
    dataSource.every((item, index) => {
      switch (typeof item) {
        case 'object':
          
          if (item && typeof item[dataSourceConfig.text] === 'string') {
            const itemText = item[dataSourceConfig.text];
            
            if (!this.props.filter(searchText, itemText, item)) break;

            const itemValue = item[dataSourceConfig.value];
            
            requestsList.push({
              text: itemText,
              value: (
                <li key={ index } value={ itemValue } className="autosuggest__suggestion" onMouseDown={ this.handleMouseDown.bind(this, itemText) }>
                  { itemText }
                </li>
              )
            });
          }
          
          break;
          
        default:
          // Do nothing
      }
      
      return !(maxSearchResults && maxSearchResults > 0 && requestsList.length === maxSearchResults);
    });
    
    this.requestsList = requestsList;
    
    return (
      <div className={ 'autosuggest' + (isOpen && requestsList.length ? ' autosuggest--open' : null) }>

        <input 
          ref='searchTextField'
          className='autosuggest__input'
          type='text'
          autoComplete='off'
          value={ searchText }
          onChange={ this.handleChange.bind(this) }
          onBlur={ this.handleBlur.bind(this) }
          onFocus={ this.handleFocus.bind(this) }
          onKeyDown={ this.handleKeyDown.bind(this) }    
        />

        {
          isOpen && requestsList.length ?
            <ul className="autosuggest__suggestions-container">
              {
                requestsList.map((item) => item.value)
              }
            </ul>
            : null
        }

      </div>
    )
  }
  
  blur() {
    this.refs.searchTextField.blur();
  }

  focus() {
    this.refs.searchTextField.focus();
  }

  close() {
    this.setState({
      open: false
    });
  }

};