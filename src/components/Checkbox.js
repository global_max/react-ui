import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import { uniqueId } from 'lodash';

class Checkbox extends Component {

	static propTypes = {
		checked: PropTypes.bool,
		defaultChecked: PropTypes.bool,
		name: PropTypes.string,
		value: PropTypes.string.isRequired,
		selectedValue: PropTypes.string,
		onChange: PropTypes.func,
		id: PropTypes.string,
		className: PropTypes.string,
		disabled: PropTypes.bool
	};

	static defaultProps = {

	};

	constructor(props, context) {
		super(props, context);
	}

	componentDidMount() {

	}

	render() {
		const { className, children, disabled, id = uniqueId('checkbox'), name, selectedValue, onChange, ...others } = this.props;
		const optional = {};

		if (selectedValue !== undefined) {
			optional.checked = (this.props.value === selectedValue);
		}

		if (typeof onChange === 'function') {
			optional.onChange = onChange.bind(null, this.props.value);
		}

		return (
			<div className={ classNames('checkbox', { [className]: className }) }>
				<input
					{ ...others }
					type="checkbox"
					id={ id }
					name={ name }
					disabled={ disabled }
					aria-disabled={ disabled }
					{ ...optional } />
				<label htmlFor={ id }>{ children }</label>
			</div>
		);
	}

}

class CheckboxGroup extends Component {

	static propTypes = {
		id: PropTypes.string,
		name: PropTypes.string.isRequired,
		onChange: PropTypes.func
	};

	static defaultProps = {

	};

	constructor(props, context) {
		super(props, context);
	}

	componentDidMount() {

	}

	render() {
		const { name, children, onChange, ...others } = this.props;

		var props = { ...others, className: 'checkbox-group' };

		return <div { ...props } >
			{
				React.Children.map(children,
					(child) => React.cloneElement(child, { name, onChange: onChange })
				)
			}
		</div>
	}
}

export { Checkbox, CheckboxGroup };