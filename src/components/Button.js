import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

const BUTTON_SIZES = ['large', 'small'];

const BUTTON_TYPES = [
	'default',
	'primary',
	'success',
	'warning',
	'danger',
	'link'
];

export default class Button extends Component {

	static propTypes = {
		block: PropTypes.bool,
		disabled: PropTypes.bool,
		className: PropTypes.string,
		href: PropTypes.string,
		isActive: PropTypes.bool,
		size: PropTypes.oneOf(BUTTON_SIZES),
		submit: PropTypes.bool,
		type: PropTypes.oneOf(BUTTON_TYPES)
	};

	static defaultProps = {
		type: 'default'
	};

	constructor(props, context) {
		super(props, context);
	}

	componentDidMount() {

	}

	render() {
		const { block, disabled, children, className, isActive, size, submit, type } = this.props;

		const componentClass = classNames('button', {
			[`button--${ type }`]: type,
			[`button--${ size }`]: size,
			'button--block': block,
			'button--disabled': disabled,
			'is-active': isActive,
			[className]: className
		});

		let props = { ...this.props, className: componentClass };
		let tag = 'button';

		props.type = submit ? 'submit' : 'button';

		if (props.href) {
			tag = 'a';
			delete props.type;
		}

		return React.createElement(tag, props, children);
	}

}