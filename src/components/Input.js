import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

export default class Input extends Component {

	static propTypes = {
		autofocus: PropTypes.bool,
		className: PropTypes.string,
		disabled: PropTypes.bool,
		id: PropTypes.string,
		name: PropTypes.string,
		onChange: PropTypes.func,
		placeholder: PropTypes.string,
		size: PropTypes.oneOf(['large', 'small']),
		type: PropTypes.string,
		value: PropTypes.oneOfType([
			PropTypes.number,
			PropTypes.string
		])
	};

	static defaultProps = {
		type: 'text'
	};

	constructor(props, context) {
		super(props, context);
	}

	componentDidMount() {

	}

	render() {
		const { className, size, placeholder } = this.props;

		const componentClass = classNames('input', {
			[`input--${ size }`]: size,
			[className]: className
		});

		let props = { ...this.props, className: componentClass, 'aria-label': placeholder, ref: 'input' };

		return <input { ...props } />
	}
}