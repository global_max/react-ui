import React, { Component } from 'react';
import Button from '../components/Button';
import { Radio, RadioGroup } from '../components/Radio';
import { Checkbox, CheckboxGroup } from '../components/Checkbox';
import AutoSuggest from '../components/AutoSuggest';
import Input from '../components/Input';

export default class App extends Component {

	constructor(props, context) {
		super(props, context);

		this.state = {
			radio: [
				{ name: 'radio', value: '1', label: 'Пример radio кнопки' },
				{ name: 'radio', value: '2', label: 'Пример radio кнопки' }
			],
			checkbox: [
				{ value: 1, label: 'Кнопка' },
				{ value: 2, label: 'Кнопка' }
			],
			autosuggestValue: '',
			autosuggest: [
				{
					name: 'C',
					year: 1972
				},
				{
					name: 'C#',
					year: 2000
				},
				{
					name: 'C++',
					year: 1983
				},
				{
					name: 'Clojure',
					year: 2007
				},
				{
					name: 'Elm',
					year: 2012
				},
				{
					name: 'Go',
					year: 2009
				},
				{
					name: 'Haskell',
					year: 1990
				},
				{
					name: 'Java',
					year: 1995
				},
				{
					name: 'Javascript',
					year: 1995
				},
				{
					name: 'Perl',
					year: 1987
				},
				{
					name: 'PHP',
					year: 1995
				},
				{
					name: 'Python',
					year: 1991
				},
				{
					name: 'Ruby',
					year: 1995
				},
				{
					name: 'Scala',
					year: 2003
				}
			]
		}
	}

	componentDidMount() {

	}

	handleRadioChange(value) {
		const { radio } = this.state;

		radio.forEach(item => {
			item.checked = item.value == value;
		});
		//radio.find(item => item.value == value);

		this.setState({
			radio: radio
		})
	}

	handleCheckboxChange(value) {
		const { checkbox } = this.state;

		checkbox.forEach(item => {
			if (item.value == value)
			item.checked = true;
		});

		this.setState({
			checkbox: checkbox
		})
	}

	handleAutosuggestChange(newValue) {
		console.log(newValue);
		this.setState({
			autosuggestValue: newValue
		});
	}

	render() {
		const { radio, autosuggest, autosuggestValue } = this.state;

		const inputProps = {
			placeholder: `Type 'c'`,
			autosuggestValue,
			onChange: this.handleAutosuggestChange.bind(this)
		};

		return (
			<div className="l-wrapper">

				<div className="b-section">
					<h3 className="b-section__title">Buttons</h3>
					<div className="b-section__container">
						<Button>Button</Button>
						<Button disabled={ true }>Disabled button</Button>
						<Button href='#'>Link</Button>
					</div>
				</div>

				<div className="b-section">
					<h3 className="b-section__title">Radio</h3>
					<div className="b-section__container">
						<RadioGroup name='radio-group' onChange={ this.handleRadioChange.bind(this) }>
							{
								radio.map((item, index) => {
									return <Radio key={ index } value={ item.value }>{ item.label }</Radio>
								})
							}
						</RadioGroup>
					</div>
				</div>

				<div className="b-section">
					<h3 className="b-section__title">Checkbox</h3>
					<div className="b-section__container">
						<CheckboxGroup name='checkbox-group' onChange={ this.handleCheckboxChange.bind(this) }>
							<Checkbox value='1'>Пример checkbox кнопки</Checkbox>
							<Checkbox value='2'>Пример checkbox кнопки</Checkbox>
						</CheckboxGroup>
					</div>
				</div>

				<div className="b-section">
					<h3 className="b-section__title">Autosuggest</h3>
					<div className="b-section__container">
						<AutoSuggest dataSource={ autosuggest } dataSourceConfig={ {text: 'name', value: 'year'} } />
					</div>
				</div>

			</div>
		);
	}
}